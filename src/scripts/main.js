$(document).ready(function() {
   // alert('OK'); 
});

/*

Variable mutation and Type coercion

*/

// Type coercion

var first_name = 'John';
var age = 28;

console.log(first_name + "  " + age);

var job, is_married;
job = "Engineer";
is_married = false;

console.log(first_name + " is a " + age + " years old " + job + " and is he married " + is_married);

// Variable mutation

age = "Twenty eight";
job = "Driver";

alert(first_name + " is a " + age + " years old " + job + " and is he married " + is_married);

// Staged Vs Unstaged Changes in GIT

console.log("I'm staged");

console.log("I'm unstaged");

// Falsy values in JS => undefined, null, 0, '', NaN

// Truthy values in JS => NOT Falsy values

var height = 0;

if(height || height === 0) {
    console.log('Height is defined')
} else {
    console.log('Height not defined');
}

console.log(typeof(height));