// Function statements vs Function declaration

var my_fn = function logSomething(name) {
    console.log('Hi ' + name);
}

my_fn('Jon');