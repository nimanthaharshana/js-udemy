var john_scores = [89, 120, 103];
var mike_scores = [116, 94, 123];
var mary_scores = [97, 134, 105];

var john_average = john_scores.reduce(function(total, value) { return total + value; }, 0) / john_scores.length;

var mike_average = mike_scores.reduce(function(total, value) { return total + value; }, 0) / mike_scores.length;

var mary_average = mary_scores.reduce(function(total, value) { return total + value; }, 0) / john_scores.length;

// Average scores

console.log("John's team aveerage : ", john_average);
console.log("Mike's team aveerage : ", mike_average);

// Who Wins ?

if(john_average > mike_average) {
    console.log("John wins ", john_average);
} else {
    console.log("Mike wins ", mike_average);
}