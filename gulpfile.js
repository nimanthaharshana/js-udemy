var gulp = require('gulp');

var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');

var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-minify');
var concat = require('gulp-concat');

var livereload = require('gulp-livereload');
var browserSync = require('browser-sync').create();

var sass = require('gulp-sass');

var uglify = require('gulp-uglify');

gulp.task('imagemin', function(done) {
   var img_src = 'src/images/**/*', img_dest = 'build/images';

   gulp.src(img_src)
   .pipe(changed(img_dest))
   .pipe(imagemin())
   .pipe(gulp.dest(img_dest));
    
    done();
});

gulp.task('styles', function(done) {
   gulp.src(['src/styles/*.scss'])
   .pipe(sass())
   .pipe(concat('styles.min.css'))
   .pipe(autoprefix('last 2 versions'))
   .pipe(minifyCSS())
   .pipe(gulp.dest('build/styles/'))
   .pipe(browserSync.stream())
   //.pipe(livereload({ start: true }));
    
    done();
});

gulp.task('scripts', function(done) {
   gulp.src(['src/scripts/*.js'])
   .pipe(concat('scripts.min.js'))
   .pipe(uglify())
   //.pipe(minifyJS())
   .pipe(gulp.dest('build/scripts/'))
   .pipe(browserSync.stream())
   //.pipe(livereload({ start: true }));
    
    done();
});

gulp.task('default', gulp.series('imagemin', 'styles',function(done) {
    console.log('Gulp default');
    done();
}));

gulp.task('watchstyles', function() {
   // watch for CSS changes
   gulp.watch('src/styles/*.scss', gulp.series('styles'));
});

gulp.task('browserSync', function() {
   browserSync.init({
      server: {
         baseDir: './'
      },
   })
    
    gulp.watch('src/styles/*.scss', gulp.series('styles'));
    gulp.watch('src/scripts/*.js', gulp.series('scripts'));
    gulp.watch('./*.html').on('change', browserSync.reload);
})